import json
import logging
import os
import shutil
import time

import requests


def get_scryfall_json(pages=None, filename=None):
    '''
    Download specified number of pages from scryfall cards endpoint to json.
    Default is to download all pages, but an optional parameter lets you
    specify how many pages to download.
    '''
    base_url = 'https://api.scryfall.com/cards?'
    filename = filename if filename else 'scryfall_data.json'
    with open(filename, 'w') as f:
        data = []
        if pages:
            for i in range(1, pages):
                request_str = '%spage=%s' % (base_url, str(i))
                r = requests.get(request_str)
                data.append(r.json())
                time.sleep(0.2)
        else:
            next_url = base_url
            while True:
                r = requests.get(next_url)
                page = r.json()
                data.append(page)
                if page['has_more'] is True:
                    next_url = page['next_page']
                else:
                    break
                time.sleep(0.2)
        json.dump(data, f, indent=4, sort_keys=True)


def call_scryfall_cards_search(*args, filename=None, include_all_prints=True):
    '''
    Hits the card search endpoint with any number of query arguments and
    optionally saves the json to a file.
    See https://scryfall.com/docs/api/cards/search for further reference
    '''
    base_url = 'https://api.scryfall.com/cards/search?'
    query = 'q='
    if include_all_prints:
        query = 'unique=prints&' + query
    for param in args:
        query = query + '%s' % param
    req_str = base_url + query
    print(req_str)
    data = paginate_request(req_str, [])
    if filename:
        with open(filename, 'w') as f:
            json.dump(data, f, indent=4, sort_keys=True)
    else:
        return data
        

def paginate_request(url, data, sleep_time=0.2):
    next_url = url
    while True:
        r = requests.get(next_url)
        page = r.json()
        data.append(page)
        if page['has_more'] is True:
            next_url = page['next_page']
        else:
            break
        time.sleep(0.2)
    return data


def download_images(filename, path=None):
    '''
    Parses given file and downloads card images specified within the file.
    '''
    image_directory = path if path else 'card_images'
    card_sets = set()
    all_json = None
    with open(filename, 'r') as f:
        all_json = json.load(f)
    for page in all_json:
        for card in page['data']:
            if 'name' in card:
                full_path = ''
                card_name = '%s - %s.jpg' % (card['name'], card['id'])
                sanitized_card_name = remove_forward_slashes(card_name)
                if 'set' in card:
                    if card['set'] not in card_sets:
                        card_sets.add(card['set'])
                        path = os.path.join(image_directory, card['set'])
                        os.makedirs(path, exist_ok=True)
                    full_path = os.path.join(image_directory, card['set'],
                                             sanitized_card_name)
                else:
                    full_path = os.path.join(image_directory,
                                             sanitized_card_name)
                if 'image_uris' in card:
                    if not os.path.exists(full_path):
                        image_link = card['image_uris']['normal']
                        r = requests.get(image_link, stream=True)
                        try:
                            with open(full_path, 'wb') as f:
                                shutil.copyfileobj(r.raw, f)
                            logging.info('done saving %s at %s' %
                                         (card['name'], full_path))
                        except Exception:
                            logging.error('Error saving file at %s'
                                          % full_path, exc_info=True)
                        time.sleep(0.2)
                    else:
                        logging.info('skipping %s because it already exists'
                                     % card['name'])
                else:
                    logging.info('card %s has no image' % card['name'])
            else:
                logging.info('no name in card')


def remove_forward_slashes(name):
    return name.replace('//', 'backslash')


logging.basicConfig(format='%(levelname)s %(asctime)s %(message)s',
                    filename='ingest.log', level=logging.DEBUG)
